package cat.ioc.bubblesort;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

/**
 * Ordenació amb métode bubblesort
 *
 * @author Javier del Moral Asensio
 * @version 1.0
 */
public class BubbleSort {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.print("Introdueix el nombre d'elements de l'array: ");
        int n = sc.nextInt();
        int[] arrayIteracio = new int[n];
        int[] arrayRecurcio = new int[n];

        Random rand = new Random();
        for (int i = 0; i < n; i++) {
            int x = rand.nextInt(100);
            arrayIteracio[i] = x;
            arrayRecurcio[i] = x;
        }

        System.out.println("Array desordenat: " + Arrays.toString(arrayIteracio));

        long tempsIniciArrayIteracio = System.currentTimeMillis();
        bubbleSortIterative(arrayIteracio);
        long tempsFiArrayIteracio = System.currentTimeMillis();
        long tempsArrayIteracio = tempsFiArrayIteracio - tempsIniciArrayIteracio;
        System.out.println("Temps d'execució de l'algorisme de la bombolla iteratiu: " + tempsArrayIteracio + " ms");
        System.out.println("Array ordenat de forma iterativa: " + Arrays.toString(arrayIteracio));
    }

    public static void bubbleSortIterative(int[] array) {
        int n = array.length;
        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - i - 1; j++) {
                if (array[j] > array[j + 1]) {
                    int temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
    }
}
